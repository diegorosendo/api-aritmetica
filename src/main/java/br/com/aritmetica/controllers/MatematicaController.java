package br.com.aritmetica.controllers;

import br.com.aritmetica.DTOs.EntradaDTO;
import br.com.aritmetica.DTOs.RespostaDTO;
import br.com.aritmetica.services.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/matematica")
public class MatematicaController {

    @Autowired
    private MatematicaService matematicaService;

    @PutMapping("/soma")
    public RespostaDTO soma(@RequestBody EntradaDTO entradaDTO){

        if (entradaDTO.getNumeros().size() <=1)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Informe ao menos dois números para serem somados");
        }

        if (!apenasNumerosNaturais(entradaDTO.getNumeros()))
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Informe apenas numeros naturais");
        }

        RespostaDTO resposta = matematicaService.soma(entradaDTO);
        return resposta;
    }

    @PutMapping("/subtracao")
    public RespostaDTO subtracao(@RequestBody EntradaDTO entradaDTO){

        if (entradaDTO.getNumeros().size() <=1)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Informe ao menos dois números para serem subtraidos");
        }

        if (!apenasNumerosNaturais(entradaDTO.getNumeros()))
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Informe apenas numeros naturais");
        }


        RespostaDTO resposta = matematicaService.subtracao(entradaDTO);
        return resposta;
    }


    @PutMapping("/multiplicacao")
    public RespostaDTO multiplicacao(@RequestBody EntradaDTO entradaDTO){

        if (entradaDTO.getNumeros().size() <=1)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Informe ao menos dois números para serem multiplicados");
        }

        if (!apenasNumerosNaturais(entradaDTO.getNumeros()))
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Informe apenas numeros naturais");
        }

        RespostaDTO resposta = matematicaService.multiplicacao(entradaDTO);
        return resposta;
    }


    @PutMapping("/divisao")
    public RespostaDTO divisao(@RequestBody EntradaDTO entradaDTO){

        if (entradaDTO.getNumeros().size() <=1)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Informe ao menos dois números para serem divididos");
        }

        if (!apenasNumerosNaturais(entradaDTO.getNumeros()))
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Informe apenas numeros naturais");
        }

        if (!apenasListaDecrescente(entradaDTO.getNumeros()))
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Informe lista de numeros naturais ordenacao decrescente");
        }


        RespostaDTO resposta = matematicaService.divisao(entradaDTO);
        return resposta;
    }



    private boolean apenasNumerosNaturais(List<Integer> numeros){
        for (Integer numero: numeros) {
            if (numero < 0){
                return false;
            }
        }
        return true;
    }

    private boolean apenasListaDecrescente(List<Integer> numeros){

        for (int i=1; i < numeros.size(); i++){
            if (numeros.get(i-1) < numeros.get(i)){
                return false;
            }
        }
        return true;
    }

}
