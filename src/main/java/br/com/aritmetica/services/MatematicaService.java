package br.com.aritmetica.services;

import br.com.aritmetica.DTOs.EntradaDTO;
import br.com.aritmetica.DTOs.RespostaDTO;
import br.com.aritmetica.controllers.MatematicaController;
import org.springframework.stereotype.Service;

@Service
public class MatematicaService{


    public RespostaDTO soma(EntradaDTO entradaDTO){
        int numero = 0;
        for (int n: entradaDTO.getNumeros()
        ) {
            numero += n;
        }
        RespostaDTO respostaDTO = new RespostaDTO();
        respostaDTO.setResultado(numero);
        return respostaDTO;
    }


    public RespostaDTO subtracao(EntradaDTO entradaDTO){
        int numero = entradaDTO.getNumeros().get(0);
        for (int i=1; i < entradaDTO.getNumeros().size(); i++){
            numero -= entradaDTO.getNumeros().get(i);
        }
        RespostaDTO respostaDTO = new RespostaDTO();
        respostaDTO.setResultado(numero);
        return respostaDTO;
    }

    public RespostaDTO multiplicacao(EntradaDTO entradaDTO){
        int numero = entradaDTO.getNumeros().get(0);
        for (int i=1; i < entradaDTO.getNumeros().size(); i++){
            numero *= entradaDTO.getNumeros().get(i);
        }
        RespostaDTO respostaDTO = new RespostaDTO();
        respostaDTO.setResultado(numero);
        return respostaDTO;
    }

    public RespostaDTO divisao(EntradaDTO entradaDTO){
        int numero = entradaDTO.getNumeros().get(0);
        for (int i=1; i < entradaDTO.getNumeros().size(); i++){
            numero /= entradaDTO.getNumeros().get(i);
        }
        RespostaDTO respostaDTO = new RespostaDTO();
        respostaDTO.setResultado(numero);
        return respostaDTO;
    }

}
